# This script takes NFL Quarterback data from 2023 CSV and transfers to a SQLite database

import csv
import sqlite3

# Global list
data_list = list()

# Extract CSV data
with open('Stats.csv', newline='') as csv_data:
    read_data = csv.reader(csv_data, delimiter=',')
    for row in read_data:
        data_list.append(row)

# Proper headings
headings = ['Rank', 'Player', 'Team', 'Age', 'Position', 'Games_played', 'Games_started', 'Record', 'Completions',
            'Attempts', 'Percent', 'Yards', 'Tds', 'Percent_tds', 'Interceptions', 'Int_percent', 'First_downs',
            'Success_percent', 'Long', 'Yards_per_attempt', 'Adjusted_ypa', 'Yards_per_completion',
            'Yards_per_game', 'Rating', 'ESPN_QBR', 'Sacked', 'Sack_yards', 'Sack_percent',
            'Net_yards_per_attempt', 'Adjusted_nypa', 'Comebacks', 'Game_winning_drives']

# Replace heading
data_list[0] = headings

# Remove garbage line
del data_list[1]

# Convert dates to record w-l-t
for line in data_list:
    if line[7].__contains__('/'):
        record = line[7].replace('/', '-', 2)
        record = record.replace('200', '')
        line[7] = record

# for line in data_list:
#    print(line)

# SQL stuff
connection = sqlite3.connect('Stats.db')
cursor = connection.cursor()

create_table_statement = """
                        CREATE TABLE IF NOT EXISTS stats(
                            Rank INTEGER,
                            Player VARCHAR(50),
                            Team VARCHAR(50),
                            Age INTEGER,
                            Position VARCHAR(10),
                            Games_played INTEGER,
                            Games_started INTEGER,
                            Record VARCHAR(10),
                            Completions INTEGER,
                            Attempts INTEGER,
                            Percent NUMBER(5,1),
                            Yards INTEGER,
                            Tds INTEGER,
                            Percent_tds NUMBER(5,1),
                            Interceptions INTEGER,
                            Int_percent NUMBER(5,1),
                            First_downs INTEGER,
                            Success_percent NUMBER(5,1),
                            Long INTEGER,
                            Yards_per_attempt NUMBER(5,1),
                            Adjusted_ypa NUMBER(5,1),
                            Yards_per_completion NUMBER(5,1),
                            Yards_per_game NUMBER(5,1),
                            Rating NUMBER(5,1),
                            ESPN_QBR NUMBER(5,1),
                            Sacked INTEGER,
                            Sack_yards INTEGER,
                            Sack_percent NUMBER(5,1),
                            Net_yards_per_attempt NUMBER(5,2),
                            Adjusted_nypa NUMBER(5,2),
                            Comebacks INTEGER,
                            Game_winning_drives INTEGER
                        );

                        """

cursor.execute(create_table_statement)

for line in data_list[1:]:
    # catch some blank spots in the data
    if line[21] == "":
        line[21] = "0"
    if line[30] == "":
        line[30] = "0"
    if line[31] == "":
        line[31] = "0"

    # make sure the apostrophe in Aidan O'Connell doesn't mess us up
    if "'" in line[1]:
        line[1] = line[1].replace("'", "`")

    query_string_first_half = "INSERT INTO stats VALUES("
    query_string_second_half = str(line[0] + ', \'' + line[1] + '\', \'' + line[2] + '\', ' + line[3] + ', \'' + line[4] + '\', ' + line[5] + ', ' + line[6] + ', \'' + line[7] + '\', ' + line[8] + ', ' + line[9] + ',' + line[10] + ',' + line[11] + ',' + line[12] + ',' + line[13] + ',' + line[14] + ',' + line[15] + ',' + line[16] + ',' + line[17] + ',' + line[18] + ',' + line[19] + ',' + line[20] + ',' + line[21] + ',' + line[22] + ',' + line[23] + ',' + line[24] + ',' + line[25] + ',' + line[26] + ',' + line[27] + ',' + line[28] + ',' + line[29] + ',' + line[30] + ',' + line[31] + ')')
    query_string = query_string_first_half + query_string_second_half
    print(query_string)
    cursor.execute(query_string)

connection.commit()