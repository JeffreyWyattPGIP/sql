import sqlite3

def sort_by_yards(cursor):
    print("\nBy yards (Rank, Name, Team, Yards)")
    query = "SELECT Rank, Player, Team, Yards FROM stats ORDER BY Yards DESC;"
    result = cursor.execute(query)

    for line in result:
        print(line)

def sort_by_tds(cursor):
    print("\nBy TDs (Rank, Name, Team, TDs)")
    query = "SELECT Rank, Player, Team, Tds FROM stats ORDER BY Tds DESC;"
    result = cursor.execute(query)

    for line in result:
        print(line)

def sort_by_qbr(cursor):
    print("\nBy QBR (Rank, Name, Team, Position, QBR)")
    query = "SELECT Rank, Player, Team, Position, ESPN_QBR FROM stats ORDER BY ESPN_QBR DESC;"
    result = cursor.execute(query)

    for line in result:
        print(line)

def sort_by_age(cursor):
    print("\nBy age (Rank, Name, Age, Team, Yards, TDs)")
    query = "SELECT Rank, Player, Age, Team, Yards, Tds FROM stats ORDER BY Age DESC;"
    result = cursor.execute(query)

    for line in result:
        print(line)

def find_by_name(cursor):
    player_name = input("Player name: ")
    # allow search by partial name, case-insensitive
    player_name = "%" + player_name.lower() + "%"
    query = "SELECT * FROM stats WHERE LOWER(Player) LIKE ?"
    result = cursor.execute(query, (player_name,))

    for line in result:
        print(line)

def find_by_team(cursor):
    team_name = input("Team name: ")
    query = "SELECT * FROM stats WHERE Team LIKE ?"
    result = cursor.execute(query, (team_name,))

    for line in result:
        print(line)

def find_by_position(cursor):
    position = input("Position: ").upper()
    query = "SELECT * FROM stats WHERE Position LIKE ?"
    result = cursor.execute(query, (position,))

    for line in result:
        print(line)

def main():
    connection = sqlite3.connect('Stats.db')
    cursor = connection.cursor()
    exit_flag = False

    while not exit_flag:
        user_selection = input("1) Sort by yards\n2) Sort by TDs\n" \
                                +"3) Sort by QBR\n4) Sort by age\n" \
                                +"5) Find by name\n6) Find by team\n" \
                                +"7) Find by position\n" \
                                +"0) Exit\n")
        match user_selection:
            case "1": sort_by_yards(cursor)
            case "2": sort_by_tds(cursor)
            case "3": sort_by_qbr(cursor)
            case "4": sort_by_age(cursor)
            case "5": find_by_name(cursor)
            case "6": find_by_team(cursor)
            case "7": find_by_position(cursor)
            case "0": exit_flag = True
            case _: print("Response not understood.")

    connection.close()

if __name__ == "__main__":
    main()
